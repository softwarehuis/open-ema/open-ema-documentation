# Menstrual issues

A recent strong increase in heavy menstrual bleeding, was mentioned in the [British Medical Journal (BMJ)](https://www.bmj.com/content/373/bmj.n958/rr-2). It is unclear if EMA has looked into this based on their data, what happens when we do it?

```SQL
select
  reaction,
  count(distinct local_number)
from import.product_reaction 
where reaction ilike '%menstru%' 
group by reaction 
order by count(distinct local_number) desc limit 10;
```

We can see that related to products, there are `1920` reports mentioning Heavy menstrual bleeding as (one of the) reaction(s).


| reaction                   | count 
| -------------------------- | -----
| Heavy menstrual bleeding   |  1920
| Intermenstrual bleeding    |  1100
| Menstrual disorder         |  1043
| Menstruation irregular     |   957
| Menstruation delayed       |   819
| Premenstrual syndrome      |    91
| Premenstrual pain          |    18
| Menstrual discomfort       |    17
| Menstruation normal        |    10
| Menstrual cycle management |     6

 ```SQL
select
  reaction,
  count(distinct local_number)
from import.substance_reaction 
where reaction ilike '%menstru%' 
group by reaction 
order by count(distinct local_number) desc limit 10;
```

| reaction                   | count 
|--------------------------- | -----
| Heavy menstrual bleeding   | 48794
| Menstrual disorder         | 30349
| Intermenstrual bleeding    | 26426
| Menstruation irregular     | 25405
| Menstruation delayed       | 18877
| Premenstrual syndrome      |  2325
| Menstrual discomfort       |  1336
| Premenstrual pain          |   921
| Menstrual cycle management |   300
| Menstruation normal        |   294

For substances it is `48794`reports.

We want to see if any substances stick out.

 ```SQL
 select a.name, b.* from 
 (select
    --reaction,
    substance_id,
    count(distinct local_number) count
  from import.substance_reaction 
  where reaction = 'Heavy menstrual bleeding'
  group by substance_id, reaction) b
  JOIN import.substances a on a.substance_id = b.substance_id
  order by b.count desc limit 10;
```

| name                                                                                      | substance_id | count 
| ----------------------------------------------------------------------------------------- | ------------ | -----
| COVID-19 MRNA VACCINE PFIZER-BIONTECH (TOZINAMERAN)                                       |     42325700 | 25570
| COVID-19 MRNA VACCINE MODERNA (ELASOMERAN)                                                |     40983312 |  7791
| COVID-19 VACCINE ASTRAZENECA (CHADOX1 NCOV-19)                                            |     40995439 |  5062
| LEVONORGESTREL                                                                            |        19980 |  3405
| ETONOGESTREL                                                                              |        18832 |  1634
| DESOGESTREL                                                                               |        18500 |   383
| ETHINYLESTRADIOL, ETONOGESTREL                                                            |        39209 |   347
| LACTOSE                                                                                   |        25479 |   288
| DROSPIRENONE, ETHINYLESTRADIOL                                                            |        59494 |   242
| COVID-19 MRNA VACCINE PFIZER-BIONTECH ORIGINAL/OMICRON BA.1 (TOZINAMERAN, RILTOZINAMERAN) |     60123556 |   212

It seems that, given that "Heavy menstrual bleeding" totals `48794` reports for the substance table and `38423` reports from that same table are related to COVID-19 vaccines, we feel that this should be something of concern. This appears to be something that justifies further research, also by EMA.