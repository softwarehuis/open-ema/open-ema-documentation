# Ivermectine

EMA advises [against the use of Ivermectine for COVID-19 prevention treatment](https://www.ema.europa.eu/en/news/ema-advises-against-use-ivermectin-prevention-treatment-covid-19-outside-randomised-clinical-trials).

_"Although ivermectin is generally well tolerated at doses authorised for other indications, side effects could increase with the much higher doses that would be needed to obtain concentrations of ivermectin in the lungs that are effective against the virus. Toxicity when ivermectin is used at higher than approved doses therefore cannot be excluded."_

Wouldn't it be good to investigate what side effects (reactions) have been reported in EudraVigilance for Ivermectine? This chapter gives some examples of what questions could be asked to the database. Can you come up with more questions? Is Ivermectine, a product or a substance? Is it in any of the drug lists?

### Is it a product?

```SQL
select * from import.products where name ilike '%iverm%';
```

| product_id | name 
| ---------- | ----

_(0 rows)_

Ivermectine is not found.

### Is it a substance?

```SQL
select * from import.substances where name ilike '%iverm%';
```

| substance_id | name    
| ------------ | ------------
|        23261 | IVERMECTIN

_(1 row)_

Results in one row and a substance_id. When we check the substance table, we find `3015` unique reports for this substance.

```SQL
select
  'substance reports' as reported_on,
  count(distinct local_number)
from import.substance where substance_id = 23261;
```

| reported_on       | count 
| ----------------- | -----
| substance reports |  3015

_(1 row)_


### Comparing outcome

What outcomes do the reports on the Ivermectine substance have?

```SQL
select
  outcome,
  count(distinct local_number)
from
  import.substance_reaction
where substance_id = 23261 group by outcome;
```
| outcome                          | count 
| -------------------------------- | -----
| Fatal                            |   282
| Not Recovered/Not Resolved       |   461
| Not Specified                    |     4
| Recovered/Resolved               |  1661
| Recovered/Resolved With Sequelae |    24
| Recovering/Resolving             |   350
|                                  |   931


How does this compare to f.i. the outcomes for the _"COVID-19 MRNA VACCINE PFIZER-BIONTECH (TOZINAMERAN)"_" substance?

```SQL
select
  outcome,
  count(distinct local_number)
from
  import.substance_reaction
where substance_id = 42325700 group by outcome;
```

| outcome                          | count 
| -------------------------------- | -------
| Fatal                            |  13723
| Not Recovered/Not Resolved       | 354461
| Recovered/Resolved               | 357564
| Recovered/Resolved With Sequelae |  27910
| Recovering/Resolving             | 214203
|                                  | 351899

We know that there are `3015` reports on the Ivermectine substance. We know there are `1034542` reports on the Tozinameran substance with the substance_id `42325700`.
What is the percentage of _"Fatal"_ outcomes for each of these substances?

* Ivermectine: `282 / 3015 = 0.0935.. ~ 9%`
* Tozinameran: `13723 / 1034542 = 1.3264.. ~ 1%`

And for _"Recovered/Resolved"_?

* Ivermectine: `1661 / 3015 = 0.5509.. ~ 55%`
* Tozinameran: `357564 / 1034542 = 1.3264.. ~ 34%`


We have not looked at the dates of the reports and maybe we are over generalizing. But it is interesting to break this research down further. Can you think of more advanced questions you want answered in comparing substances?


### Can we find Ivermectine in the drug lists?

Is Ivermectine mentioned in any of the drug lists? Let's find out and by joining all drug_list counts for the unique number of reports.

```SQL
select 
  'product'as class,
  'suspect' as list_type,
  count(distinct local_number) as count
from import.product_subject_drug_list
where drugs ilike '%iverm%'
UNION
select 
  'product'as class,
  'concomitant' as list_type,
  count(distinct local_number) as count
from import.product_concomitant_drug_list
where drugs ilike '%iverm%'
UNION
select 
  'substance'as class,
  'suspect' as list_type,
  count(distinct local_number) as count
from import.substance_subject_drug_list
where drugs ilike '%iverm%'
UNION
select 
  'substance'as class,
  'concomitant' as list_type,
  count(distinct local_number) as count
from import.substance_concomitant_drug_list
where drugs ilike '%iverm%';
```

| class     |  list_type  | count 
| --------- | ----------- | ------
| substance | suspect     |  2993
| substance | concomitant |   695
| product   | suspect     |    51
| product   | concomitant |   136


There are mentions in the drug lists. It would be good to relate them back to substances and products.
