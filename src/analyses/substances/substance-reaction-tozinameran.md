# Top 10 reactions for Tozinameran

The number one substance we found earlier was Tozinameran. To find the top 10 reactions with their outcome for the Tozinameran substance, we first lookup the `substance_id`:

```SQL
SELECT 
  *
from import.substances
where name ilike '%TOZINAMERAN%';
```

|substance_id | name |
|-- | --|
| 42325700 | COVID-19 MRNA VACCINE PFIZER-BIONTECH (TOZINAMERAN)
| 60123556 | COVID-19 MRNA VACCINE PFIZER-BIONTECH ORIGINAL/OMICRON BA.1 (TOZINAMERAN, RILTOZINAMERAN)
| 60141237 | COVID-19 MRNA VACCINE PFIZER-BIONTECH ORIGINAL/OMICRON BA.4-5 (TOZINAMERAN, FAMTOZINAMERAN)

Let's select substance_id `42325700` and see in the reaction table for substances which reactions are related to this substance_id, we will group them by reaction and outcome to get an general idea and limit the result to 10.

```SQL
select
  reaction,
  outcome,
  count(*)
from
  import.substance_reaction
where substance_id = 42325700
GROUP BY substance_id, reaction, outcome
order by count(*) desc limit 10;
```

| reaction            |          outcome           | count 
|---------------------|----------------------------|-------
| COVID-19            |                            | 98917
| Vaccination failure |                            | 88821
| Headache            | Recovered/Resolved         | 71871
| Pyrexia             | Recovered/Resolved         | 69366
| Fatigue             | Not Recovered/Not Resolved | 49876
| Headache            | Not Recovered/Not Resolved | 49474
| Fatigue             | Recovered/Resolved         | 47029
| Myalgia             | Recovered/Resolved         | 43603
| Chills              | Recovered/Resolved         | 40605
| Headache            | Recovering/Resolving       | 34880

In `98917` reports, the reaction was **COVID-19**. In `88821`cases the vaccination **failed**.
