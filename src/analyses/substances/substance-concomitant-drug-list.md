# Substance concomitant drug list

Select the top 10 of drugs reported on for substances for the [concomitant](/misc/glossary.md#concomitant) drug list table.

```SQL
select
  drugs,
  COUNT(DISTINCT local_number)
from import.substance_concomitant_drug_list
GROUP BY drugs 
order by COUNT(DISTINCT local_number) desc
limit 10;
```

|                 drugs                  | count  
|----------------------------------------|--------
 [PARACETAMOL]                                                    | 213671
 [ACETYLSALICYLIC ACID]                                           | 205282
 [OMEPRAZOLE]                                                     | 132309
 [PREDNISONE]                                                     | 129979
 [LEVOTHYROXINE SODIUM]                                           | 127684
 [FUROSEMIDE]                                                     | 126576
 [SIMVASTATIN]                                                    | 118803
 [METFORMIN, METFORMIN HYDROCHLORIDE, METFORMIN HYDROCHLORIDE BP] | 106566
 [ATORVASTATIN]                                                   | 101405
 [FOLIC ACID]                                                     | 101267
(10 rows)


_(10 rows)_

It seems from the [suspect drug list](/analyses/substance-suspect-drug-list.md) that medicine names have no square brackets in their name and that the content between square brackets is a substance. Paracetamol comes from a lot of different brands and maybe the EMA decided to group them and omit the name. This can however not be concluded from the data.


