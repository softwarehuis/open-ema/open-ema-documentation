# Top 10 substances

```SQL
select
  a.substance_id,
  b.name,
  count(distinct a.local_number)
from import.substance a 
  join import.substances b 
  on a.substance_id = b.substance_id 
GROUP BY 
  a.substance_id,
  b.name
order by 
  count(distinct a.local_number) desc
limit 10;
```

| substance_id |   name                                                                         | count  
| ------------ | ------------------------------------------------------------------------------ | -------
|     42325700 | COVID-19 MRNA VACCINE PFIZER-BIONTECH (TOZINAMERAN)                            | 1034456
|     40995439 | COVID-19 VACCINE ASTRAZENECA (CHADOX1 NCOV-19)                                 |  445801
|     40983312 | COVID-19 MRNA VACCINE MODERNA (ELASOMERAN)                                     |  375783
|        50950 | LENALIDOMIDE                                                                   |  165351
|        15756 | INFLIXIMAB                                                                     |  124291
|        15535 | ETANERCEPT                                                                     |  121607
|        20353 | METHOTREXATE                                                                   |  110786
|        23893 | ACETYLSALICYLIC ACID                                                           |  108986
|       128670 | CALCIUM CHLORIDE, SODIUM CHLORIDE, GLUCOSE, SODIUM LACTATE, MAGNESIUM CHLORIDE |  106046
|        19980 | LEVONORGESTREL                                                                 |  102116

_(10 rows)_