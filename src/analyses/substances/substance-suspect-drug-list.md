# Substance suspect drug list

Select the top 10 of drugs reported on for substances for the suspect drug list table. This table is accidentally named subject instead of suspect.

```SQL
select
  drugs,
  COUNT(DISTINCT local_number)
from import.substance_subject_drug_list
GROUP BY drugs 
order by COUNT(DISTINCT local_number) desc
limit 10;
```

|                 drugs                                                                                | count  
|------------------------------------------------------------------------------------------------------|--------
 [COMIRNATY](https://www.ema.europa.eu/en/medicines/human/EPAR/comirnaty) [TOZINAMERAN]                | 904812
 COVID-19 VACCINE ASTRAZENECA                                                                          | 337828
 [SPIKEVAX](https://www.ema.europa.eu/en/medicines/human/EPAR/spikevax) [COVID-19 MRNA VACCINE MODERNA | 316281
 [VAXZEVRIA](https://www.ema.europa.eu/en/medicines/human/EPAR/vaxzevria) [COVID-19 VACCINE ASTRAZENECA| 204072
 [REVLIMID](https://www.ema.europa.eu/en/medicines/human/EPAR/revlimid) [LENALIDOMIDE]                 | 155625
 TOZINAMERAN [TOZINAMERAN]                                                                             | 117032
 [ENBREL](https://www.ema.europa.eu/en/medicines/human/EPAR/enbrel) [ETANERCEPT]                       | 101998
 [CLOZAPINE]                                                                                           |  88094
 [LEVONORGESTREL]                                                                                      |  82590
 [REMICADE](https://www.ema.europa.eu/en/medicines/human/EPAR/remicade) [INFLIXIMAB]                   |  73876

_(10 rows)_

_Links have been added to the entries in the [EPAR](https://www.ema.europa.eu/en/medicines/what-we-publish-when/european-public-assessment-reports-background-context) information from the EMA website._

_"COMIRNATY contains tozinameran, a messenger RNA"_ as stated [here](https://www.ema.europa.eu/en/medicines/human/EPAR/comirnaty) and we find TOZINAMERAN as a substance in the substances list too.

It is interesting to see that most reports are related to a COVID-19 drug, namely the first four entries plus the sixth. From the top-10, the sum of all reports related a COVID-19 drug totals `1880025` if we relate this to the total number of [unique reports](/analyses/unique-reports.md): `10210452`, about 20% of all reports are COVID-19 medicine related.

Another interesting medicine in the top ten list of most reported drugs, is [REVLIMID](https://www.ema.europa.eu/en/medicines/human/EPAR/revlimid) which is a medicine used for the treatment of certain cancers and serious conditions affecting blood cells and bone marrow, namely multiple myeloma, myelodysplastic syndromes and mantle cell and follicular lymphoma.
