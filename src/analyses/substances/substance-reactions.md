# Top 10 reactions

## Reactions

Let's look at the reaction that are present to find the reactions with most reports.

```SQL
select
  reaction,
  count(*)
from
  import.substance_reaction
GROUP BY reaction   
order by count(*) desc limit 10;
```

|reaction         | count  
|-----------------|--------
| Pyrexia          | 840351
| Headache         | 831209
| Fatigue          | 752054
| Nausea           | 656244
| Drug ineffective | 564964
| Dyspnoea         | 481568
| Diarrhoea        | 433502
| Arthralgia       | 430680
| Rash             | 429196
| Dizziness        | 427772


Let's do another check and find the reactions with most reports and segregate by outcome

```SQL
select
  reaction,
  outcome,
  count(*)
from
  import.substance_reaction
GROUP BY reaction, outcome
order by count(*) desc limit 10;
```

|reaction     |          outcome           | count  
|------------------|----------------------------|--------
 Pyrexia          | Recovered/Resolved         | 418006
 Drug ineffective |                            | 402969
 Off label use    |                            | 302933
 Headache         | Recovered/Resolved         | 300059
 Death            | Fatal                      | 292108
 Fatigue          | Not Recovered/Not Resolved | 248051
 Nausea           | Recovered/Resolved         | 241111
 Fatigue          |                            | 208296
 Headache         | Not Recovered/Not Resolved | 201347
 Nausea           |                            | 191709

 It is interesting to see that **Death** which has the outcome **Fatal** enters the top 10.

## Fatal outcome

Let's find a top 10 with **Fatal** outcome.

 ```SQL
select
  reaction,
  outcome,
  count(*)
from
  import.substance_reaction
where
  outcome ilike '%fatal%'
GROUP BY reaction, outcome
order by count(*) desc limit 10;
```

| reaction               | outcome | count  
|-------------------------------------|---------|--------
 Death                               | Fatal   | 292108
 Completed suicide                   | Fatal   |  81474
 Toxicity to various agents          | Fatal   |  67831
 Cardiac arrest                      | Fatal   |  34689
 Pneumonia                           | Fatal   |  27633
 Sepsis                              | Fatal   |  27031
 Drug abuse                          | Fatal   |  26598
 Malignant neoplasm progression      | Fatal   |  23101
 Multiple organ dysfunction syndrome | Fatal   |  22958
 Respiratory failure                 | Fatal   |  21990

## Death is not always fatal

As extra analyses, we group the reports where the reaction contains the word **death** we assume not all reporting has been entered correct as it seems possible to have death as a reaction where the outcome is not equal to "Fatal". An interesting combination of reaction and outcome is "Recovered/Resolved" from "Foetal death". Foetal death is a synonym for "still birth", the death of a foetus needs not be fatal for the mother, whilst it is for the unborn child.

```SQL
select
  reaction,
  outcome,
  count(*)
from
  import.substance_reaction
where
  reaction ilike '%death%'
GROUP BY reaction, outcome
order by count(*) desc limit 25;
```

| reaction                            |          outcome           | count  
--------------------------------------|----------------------------|--------
 Death                                | Fatal                      | 292108
 Sudden death                         | Fatal                      |   9284
 Near death experience                |                            |   2844
 Death                                | Not Specified              |   2456
 Foetal death                         |                            |   2341
 Sudden cardiac death                 | Fatal                      |   2090
 Sudden infant death syndrome         | Fatal                      |   1850
 Foetal death                         | Fatal                      |   1824
 Death                                |                            |   1786
 Death neonatal                       | Fatal                      |   1652
 Brain death                          | Fatal                      |   1428
 Fear of death                        |                            |   1198
 Death                                | Not Recovered/Not Resolved |   1171
 Accidental death                     | Fatal                      |    917
 Cardiac death                        | Fatal                      |    809
 Foetal death                         | Recovered/Resolved         |    754
 Apparent death                       | Recovered/Resolved         |    681
 Sudden cardiac death                 |                            |    641
 Cell death                           |                            |    562
 Near death experience                | Recovered/Resolved         |    560
 Cell death                           | Recovered/Resolved         |    544
 Sudden death                         |                            |    497
 Fear of death                        | Recovered/Resolved         |    464
 Sudden unexplained death in epilepsy | Fatal                      |    461
 Foetal death                         | Not Recovered/Not Resolved |    446

_(25 rows)_