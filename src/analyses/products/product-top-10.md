# Top 10 products

```SQL
select
  a.product_id,
  b.name,
  count(distinct a.local_number)
from import.product a 
  join import.products b 
  on a.product_id = b.product_id 
GROUP BY 
  a.product_id,
  b.name
order by 
  count(distinct a.local_number) desc
limit 10;
```

| product_id |   name   | count  
|------------|----------|--------
|      77936 | REVLIMID | 157288
|      45328 | ENBREL   | 108091
|      58535 | REMICADE |  76957
|      58830 | HUMIRA   |  73708
|     176663 | ELIQUIS  |  64894
|      86304 | PRADAXA  |  51304
|      47388 | LYRICA   |  49658
|      58467 | AVASTIN  |  39258
|      49090 | PLAVIX   |  34943
|   12129169 | KEYTRUDA |  34024

