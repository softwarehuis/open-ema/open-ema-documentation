# Product literature

To see if we can get valuable information from the literature, we can use the PostgreSQL [text search features](https://www.postgresql.org/docs/current/textsearch-features.html) functionality.

We can gather statistics about the most used words. We use "english" as the dictionary, this will remove common words like "the", "and", "is" from the results.

```SQL
SELECT word, ndoc
   FROM ts_stat($$
     SELECT to_tsvector('english', literature)
     FROM import.product_literature AS t
  $$)
  where LENGTH(word) > 4
  order by
    ndoc desc,
    LENGTH(word) desc
  limit 10;
```

| word      | ndoc  
| --------- |-------
| journal   | 28961
| patient   | 28746
| societi   | 24586
| japanes   | 22833
| therapi   | 18066
| treatment | 17515
| cancer    | 15571
| annual    | 15029
| clinic    | 14086
| literatur | 12188

_(10 rows)_


We can also search for literature references containing one ore more specific words, f.i. "covid". let's do that and find the 10 most referenced literature with the number of reports referencing them. As we will see, it is hard to detect any pattern, but there are more advanced ways to seek through text in PostgreSQL and we encourage everyone that is seriously interested in literature used in reports to look at the [PostgreSQL documentation](https://www.postgresql.org/docs/current/textsearch-intro.html) on text search.

```SQL
select
  trim(literature),
  COUNT(DISTINCT local_number)
from import.product_literature
where
  to_tsvector('simple', literature) @@ to_tsquery('simple', 'covid')
GROUP BY trim(literature)
order by COUNT(DISTINCT local_number) desc limit 10;
```

| Literature | count |
| -- | -- | 
| Lopez G, Valero Zanuy M, Barrios I, et.al.. [Acute hypertriglyceridemia in patients with covid-19 receiving parenteral nutrition](https://pubmed.ncbi.nlm.nih.gov/34371797/). Nutrients. 2021;13 (7):. | 29
| Hoek A S R, Manintveld C O, Betjes G H M, Hellemons E M, Seghers L, Van Kampen A A J, et al.. [COVID-19 in solid organ transplant recipients: a single-center experience](https://pubmed.ncbi.nlm.nih.gov/32460390/). Transplant International. 2020;33:1099–1105|    21
| Elikowski W, Fertala N, Zawodna-Marszalek M, Rajewska-Tabor J, Swidurski W, Wajdlich D, Marszalek A, Skrzywanek P, Pyda M, Zytkiewicz M. [Marked self-limiting sinus bradycardia in COVID-19 patients not requiring therapy in the intensive care unit(https://pubmed.ncbi.nlm.nih.gov/34464372/)]|    12
| Aouba A, Baldolli A, Geffray L, Verdon R, Bergot E, Martin-Silva N, et al. [Targeting the inflammatory cascade with anakinra in moderate to severe COVID-19 pneumonia: case series](https://pubmed.ncbi.nlm.nih.gov/32376597/). Ann Rheum Dis. 2020;1-2. |     9
| Beyls C, Martin N, Hermida A, Et.al. [Lopinavir-Ritonavir Treatment for COVID-19 Infection in Intensive Care Unit](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7446985/). Circulation: Arrhythmia and Electrophysiology. 2020 AUG;13(E008798):862-865. |     9
| Saez-Gimenez B, Berastegui C, Barrecheguren M, Revilla-Lopez E, Los Arcos I, Alonso R, Aguilar M et.al.. [COVID-19 in lung transplant recipients: A multicenter study](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9800491/).. Am J Transplant.. 2021;21:1816-24 |     9
| Omidi N, Forouzannia SK, Poorhosseini H, Tafti S.H.A, Salehbeigi S, Lotfi-Tokaldany M. [Prosthetic heart valves and the COVID-19 pandemic era: What should we be concerned about?](https://pubmed.ncbi.nlm.nih.gov/33043651/). Journal of cardiac surgery. 2020;35(10) 2500-5. doi:10.1111/jocs.14707 |     8
| Elec A, Elec F, Oltean M, et. al.. [COVID-19 after kidney transplantation: Early outcomes and renal function following antiviral treatment](https://pubmed.ncbi.nlm.nih.gov/33453396/). Int J Infect Dis. 2021;104:426-432. |     8
| Giacchetti G.; Salvio G.; Gianfelice C. et al.. [Remote management of osteoporosis in the first wave of the COVID-19 pandemic](https://pubmed.ncbi.nlm.nih.gov/35235056/). Archives of Osteoporosis. 2022;17 (37):1-9 |     8
| Havlir, D.. [32nd Annual Medical Management of HIV/AIDS and COVID-19](https://www.ucsfcme.com/2021/MDM21K02/info.html). Live Stream Conference. 2020;unk:unk|     7