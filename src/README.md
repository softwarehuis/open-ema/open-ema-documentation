# Introduction

The European Medicines Agency ([EMA](https://www.ema.europa.eu)) is a decentralised agency of the European Union (EU). EMA hosts a system called EudraVigilance used for managing and analysing information on suspected adverse reactions to medicines. 

A restricted set of data elements in EudraVigilance is available and provided to healthcare professionals, patients and the general public. To allow for a more user friendly experience OpenEMA downloaded the data. 

OpenEMA is a PRIVATE entity, not affiliated with EMA. In this documentation we describe how data was downloaded from EMA and subsequently put in a database. We then show [example analyses](/analyses) that were run against the database using Standard Query Language (SQL). Finally we create [visualisations](/visualisation) for data and trends using the open source version of [Metabase](https://www.metabase.com/start/oss/).

For example; the [top-10 reported substances](/visualisation/substances/substance-top-10.html) represented in a bar chart.

![](/visualisation/substances/images/ema-substance-sql-top-10.png)

Or [substance reactions](/visualisation/substances/substance-reaction-per-month.html) visualised in a line graph.

![](/visualisation/substances/images/ema-substance-reaction-final.png)

This project is meant to share the data and help you, our audience, to reproduce our examples and kickstart a community of amateur and professional (data) scientists to use this database and inform each other and other professionals and the public on what information is available and what the information can tell us.

## About the data

We downloaded line listings for _Individual Case Safety Reports_ (ICSR, shortened throughout this documentation as **report**) and processed them into a PostgreSQL database. PostgreSQL can be queries with SQL that enables users to ask questions such as _"Select the sum of all records from a table of data where column X has a date value of Y"_. PostgreSQL has a beginner [tutorial](https://www.postgresql.org/docs/16/tutorial-sql.html) available that explains the possibilities of using SQL.

## Community

We strongly encourage everyone to share ideas and insights and open a broad discussion on the value of the EMA Eudravigilance data and the information available within the data. We welcome everyone to the OpenEMA community.

To enable this goal, the data and this documentation is made available under the [CC-BY-NC-SA](http://creativecommons.org/licenses/by-nc-sa/4.0/) license.

> Keep in mind, the examples are just that, examples. They can (and will) contain (minor) errors due to the fact that reports spanning multiple products or substances are not always correctly queried in the examples. We can use help to make sure the examples are checked, validated and corrected where necessary.