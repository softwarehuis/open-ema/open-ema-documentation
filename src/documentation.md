# This documentation

The documentation is maintained with [mdbook](https://rust-lang.github.io/mdBook/). The source code for this website is on [gitlab](https://gitlab.com/softwarehuis/open-ema/open-ema-documentation).

## Pre-compiled binaries

Executable binaries are available for download on the [GitHub Releases page](https://github.com/rust-lang/mdBook/releases). Download the binary for your platform (Windows, macOS, or Linux) and extract the archive. The archive contains an `mdbook` executable which you can run to build your books.

To make it easier to run, put the path to the binary into your `PATH`.

## Serve

You can run the documentation as a website using:

```bash
mdbook serve -p 3001 --open
```