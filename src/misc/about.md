# About

The internet domain OpenEMA.org hosts a community supported open source project and is not affiliated in any way with any European Union Agency.
The website [documents](https://docs.openema.org/) how publicly available data can be opened and analysed, such as it is presented to Healthcare Professionals and the Public on the EMA [adrreports.eu](https://www.adrreports.eu/) portal.

In the 'reports' section of the 'Analyses' chapter of [docs.openema.org](https://docs.openema.org) it is indicated that approx. 9.6 million unique reports are presented by EMA to Healthcare Professionals and the Public. In the 2022 Annual Report on EudraVigilance for the European Parliament, the Council and the Commission, however it is said there are some 15 million cases.

No conclusions can be drawn based on the subset of information that is the 9.6 million unique reports.

You can download CSV files and PostgreSQL database dumps from:

- [All available downloads](https://filedn.eu/lzdCKWTXt6e79Ns6J01mhb0/OpenEMA/)
- [CSV files and postgreSQL dump created on Jun 20, 2023](https://filedn.eu/lzdCKWTXt6e79Ns6J01mhb0/OpenEMA/2023-06-20/)
- [CSV files created on Sep 15, 2023](https://filedn.eu/lzdCKWTXt6e79Ns6J01mhb0/OpenEMA/2023-09-15/)

You may visit the European Medicines Agency website [here](https://www.ema.europa.eu/)

You may query the EudraVigilance system [here](https://www.adrreports.eu/en/index.html)
