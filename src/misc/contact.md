# Contact

We are working on a community platform. In the meanwhile, you can reach us by email at **community`@`open-ema.eu** or open an issue in our [repository](https://gitlab.com/softwarehuis/open-ema/open-ema-documentation/-/issues). To create issues on gitlab, you need a gitlab account. Please read the [gitlab privacy statement](https://about.gitlab.com/privacy/) for details about your account.

OpenEMA takes the PUBLICLY available data from EudraVigilance and makes it more user friendly. We are a PRIVATE entity, not affiliated with [EMA](https://www.ema.europa.eu).

* [Twitter](https://twitter.com/open_ema)
* [Issues](https://gitlab.com/softwarehuis/open-ema/open-ema-documentation/-/issues)

