# Glossary

> More terms are explained in the _adrreports.eu_ [Glossary](https://www.adrreports.eu/en/glossary.html)

## Adverse Drug Reaction Report (Adrr)

The adrreports.eu portal provides public access to reports of suspected side effects submitted to the
EudraVigilance system by national medicines regulatory authorities and pharmaceutical companies that
hold marketing authorisations for medicines in the European Economic Area (EEA).

_see: [Adrreports.eu](https://www.adrreports.eu)_


## Concomitant 

Synonym for "accompanying".  A descendant of Latin concomitari ("to accompany") and ultimately of "comes," the Latin word for companion.

_source: [Merriam Webster Dictionary](https://www.merriam-webster.com/dictionary/concomitant)_

## Drug

Synonym for **Medicine** as stated on [Adrreports.eu](https://www.adrreports.eu): _"Centrally authorised medicine or non-centrally authorised medicine"_

_"For centrally authorised medicines, access to reports is granted both by the name of the medicine or the name of the active substance. For non-centrally authorised medicines, access is granted based on the name of the active substance only."_

As an example: the most reported substance we find in the database is Tozinameran, Comirnaty is the product it is used in, BioNTech/Pfizer-vaccin is the drug that would be administered.

_see: [Adrreports.eu (EN)](https://www.adrreports.eu/en/index.html)

## Drug List

Information that shows characterisation of ‘Drug Role’, defined as suspect, interacting, concomitant or drug not administered. Based on this data element, 2 different ‘Drug’ (medicines) lists will be created, for suspect and interacting drugs and for concomitant or drug not administered. Reported medicinal product, displayed as recoded against the Extended EudraVigilance Medicinal Product Dictionary for centrally authorised products (for non-centrally authorised products, only the recoded substance will be displayed where reported). Substance / Specified Substance Name, displayed as recoded against the Extended EudraVigilance Medicinal Product Dictionary (if not, it will be displayed as reported). Indication of the medicinal product described as MedDRA Preferred Term. _‘Duration of Drug Administration’_, as reported or based on ‘Drug Administration Start Date’ and ‘End Date’, Dose of the medicine and Route of administration of the medicine.

_source: [Web Report User Guide](https://www.adrreports.eu/docs/Web%20report%20user%20guide%20EN.pdf)_

## EMA

Abbreviation for _"European Medicine Agency"_.

_"The European Medicines Agency (EMA) is a decentralised agency of the European Union (EU). It began operating in 1995. The Agency is responsible for the scientific evaluation, supervision and safety monitoring of medicines in the EU."_

_source: [EMA website](https://www.ema.europa.eu/en/about-us)_

## EudraVigilance

_"The system for managing and analysing information on suspected adverse reactions to medicines which have been authorised or being studied in clinical trials in the European Economic Area (EEA). The European Medicines Agency (EMA) operates the system on behalf of the European Union (EU) medicines regulatory network."_

_source: [EMA Website, EudraVigilance](https://www.ema.europa.eu/en/human-regulatory/research-development/pharmacovigilance/eudravigilance)_

## Line listing

Data downloaded from the EudraVigilance webpage.

_"The data and information provided at substance level is in the form of an electronic Reaction Monitoring Report (eRMR) containing aggregated data and a line listing with details of the individual cases. Users are also able to retrieve Individual Case Safety Report (ICSRs) forms accessible through the line listing."_

_see: [EudraVigilance User Manual Marketing Authorisation Holders](https://www.ema.europa.eu/en/documents/regulatory-procedural-guideline/eudravigilance-user-manual-marketing-authorisation-holders_en.pdf)_

## Local number

The local number is the unique identifier also known as *"Case Identifier format"* as stated on page 89, #12 of the [EU Individual Case Safety Report (ICSR)¹ Implementation Guide](https://www.ema.europa.eu/en/documents/regulatory-procedural-guideline/european-union-individual-case-safety-report-icsr-implementation-guide_en.pdf). It consists of 
> "Country code" followed by a dash, "organisation ID or name" followed by a dash and "local system number"
>
>  f.i.
>
>  `EU-EC-2950025`

In the public EMA database, Country code has been replaced by **EU** and organisation ID by **EC** (abbr. for European Commission) for all the reports.

## mdbook

A tool used for generating documentation from markdown files. [mdbook documentation website ](https://rust-lang.github.io/mdBook/)

## Postgresql

The database system we use. [PostgreSQL website](https://www.postgresql.org/)

## Report
 
Used throughout this documentation as synonym for **ICSR** or **Individual Case Safety Report**.

_"Used for reporting to the EudraVigilance database suspected adverse reactions to a medicinal product that occur in a single patient at a specific point in time"_

_see: [EU Individual Case Safety Report (ICSR)¹ Implementation Guide](https://www.ema.europa.eu/en/documents/regulatory-procedural-guideline/european-union-individual-case-safety-report-icsr-implementation-guide_en.pdf)_

## SQL

_"A domain-specific language used in programming and designed for managing data held in a relational database management system (RDBMS), or for stream processing in a relational data stream management system (RDSMS). It is particularly useful in handling structured data, i.e., data incorporating relations among entities and variables."_

_source: [Wikipedia](https://en.wikipedia.org/wiki/SQL)_

## Drug List

Information that shows characterisation of ‘Drug Role’, defined as suspect, interacting, concomitant or drug not administered. Based on this data element, 2 different ‘Drug’ (medicines) lists will be created, for suspect and interacting drugs and for concomitant or drug not administered. Reported medicinal product, displayed as recoded against the Extended EudraVigilance Medicinal Product Dictionary for centrally authorised products (for non-centrally authorised products, only the recoded substance will be displayed where reported). Substance / Specified Substance Name, displayed as recoded against the Extended EudraVigilance Medicinal Product Dictionary (if not, it will be displayed as reported). Indication of the medicinal product described as MedDRA Preferred Term. _‘Duration of Drug Administration’_, as reported or based on ‘Drug Administration Start Date’ and ‘End Date’, Dose of the medicine and Route of administration of the medicine.

_source: [Web Report User Guide](https://www.adrreports.eu/docs/Web%20report%20user%20guide%20EN.pdf)_

