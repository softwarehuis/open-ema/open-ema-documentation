# Product reaction per year

In Metabase, select the EMA database from the Home page clicking the link.

![](images/select-ema-database.png)

You will be taken to a page that shows all the tables in the database.

![](images/ema-database-overview.png)

We created the following visualisation where we:

- Summarized by _Distinct values of Local Number_
- Grouped by _Outcome_
- Aggregated the _Report date_ _by year_

The screenshot shows the result, we challenge you to create a similar presentation.

![](images/product-per-year.png)

We changed the sequence of the results and changed the colors for the bars in the left panel.

![](images/product-per-year-left-panel.png)

Did you manage to get the same presentation? Let's look at it in detail and draw example conclusions.

![](images/product-per-year-reaction.png)

A couple of things we notice:

- The number of reports on _product_ with a _Fatal_ outcome seems to be stable or even decline a little.
- The number of reports on _product_ that have an unspecified or unset outcome has been rising and kept rising until 2020 to drop a little from there.
- The number of reports on _product_ is declining since 2020
- The number of reports on _product_ with outcome _Recovering/Resolving_ and _Recovered/Resolved_ has been rising, but is declining since 2020.

Can you think of more advanced uses? We welcome discussion and suggestions, also about misinterpretation. Let us know!