# Substance reports per month

In Metabase, select the EMA database from the Home page clicking the link.

![](images/select-ema-database.png)

You will be taken to a page that shows all the tables in the database.

![](images/ema-database-overview.png)

Click the **Substance** button. You should now see the Substance table.

![](images/ema-substance-table.png)

Press the **Report Date** button above the column showing the dates and select **Distribution**

![](images/ema-substance-report-date.png)

Metabase should render a line graph.

![](images/ema-substance-report-date-graph.png)

