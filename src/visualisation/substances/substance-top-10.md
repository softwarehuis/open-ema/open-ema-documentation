# Substance top-10

In Metabase, select the EMA database from the Home page clicking the link.

![](images/select-ema-database.png)

You will be taken to a page that shows all the tables in the database.

![](images/ema-database-overview.png)

Click the **Substance** button. You should now see the Substance table.

![](images/ema-substance-table.png)

We press the **+ New** button in the top right of the screen and select **SQL query**.

![](images/ema-substance-new-sql-query.png)

And end up with a blank sheet

![](images/ema-substance-blank-sql.png)

In the input area of the page, we enter the query that we find at [analyses - substances - substance top-10](/analyses/substances/substance-top-10.md#Top%2010%20%substances)

And press the blue play button in the lower right corner.

A screen appears showing that the data is being loaded (doing science).

![](images/doing-science.png)

When the query finishes running, your screen should show the query in the top section and a table in the bottom section like in the next screenshot.

![](images/ema-substance-sql-ran.png)

You can now select the **Visualization** button that opens a side panel.

![](images/ema-substance-sql-visualisation.png)

Select the **Row** visualisation to get the picture shown below.

![](images/ema-substance-sql-visualisation-row.png)

If you hover your mouse over the now selected **Row** button, you will see a cog symbol appear. Click it. A new side panel opens with _Row options_

![](images/ema-substance-sql-visualisation-row-options.png)

Remove the **substance_id** from the X-axis

![](images/ema-substance-sql-visualisation-row-options-remove.png)

You will see the visualisation change, now showing the substance top-10 in a horizontal row chart.

![](images/ema-substance-sql-top-10.png)


