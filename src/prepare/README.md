# Introduction

After the import ran, we noticed there where duplicate records in the database in various tables, so we decided to write a SQL script to make sure all tables in the import schema contained unique records only.