
# OpenEMA
- [Introduction](./README.md)
- [This documentation](./documentation.md)
- [Install postgres](./data/database.md)

# Preparation

- [How we get line listings](data/download-line-listings.md)
- [Import line listings](data/import-line-listings.md)
- [Clean the database tables](prepare/clean.md)
- [Database content metrics](prepare/metrics.md)

# Analyses

- [Analyses](analyses/README.md)
  - [Reports](analyses/reports/README.md)
    - [Unique reports](analyses/reports/reports-unique.md)
    - [Report versions](analyses/reports/reports-versions.md)
    - [Reports per age group](analyses/reports/reports-per-age_group.md)
    - [Reports per country](analyses/reports/reports-per-country.md)
  - [Products](analyses/products/README.md)
    - [Product top-10](analyses/products/product-top-10.md)
    - [Product details](analyses/products/product-details.md)
    - [Product literature](analyses/products/product-literature-stats.md)
  - [Substances](analyses/substances/README.md)
    - [Substance top-10](analyses/substances/substance-top-10.md)
    - [Substance and region](analyses/substances/substance-and-region.md)
    - [Substance reactions](analyses/substances/substance-reactions.md)
      - [Reactions for Tozinameran](analyses/substances/substance-reaction-tozinameran.md)
    - [Substance suspect drug list](analyses/substances/substance-suspect-drug-list.md)
    - [Substance concomitant drug list](analyses/substances/substance-concomitant-drug-list.md)
  - [Advanced](analyses/advanced/README.md)
    - [Menstrual issues](analyses/advanced/menstrual.md)
    - [Product, Substance, Drug](analyses/advanced/drug-substance-product.md)
    - [Ivermectine](analyses/advanced/ivermectine.md)

# Visualisation
- [Installation](visualisation/README.md)
  - [Substance](visualisation/substances/README.md)
    - [Substance top-10](visualisation/substances/substance-top-10.md)
    - [Substance reports per month](visualisation/substances/substance-reports-per-month.md)
    - [Substance reaction per month](visualisation/substances/substance-reaction-per-month.md)
  - [Product](visualisation/products/README.md)
    - [Product reaction per year](visualisation/products/product-reaction-per-year.md)
# References

- [Glossary](misc/glossary.md)
- [Database schema](data/schema.md)
- [Contact](misc/contact.md)
- [About](misc/about.md)